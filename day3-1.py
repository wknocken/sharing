import re
filePath = "C:\\Users\Bill\Downloads\input 4.txt"
file1 = open(filePath, "r")
file = file1.read().split("\n\n")
file1.close()

def check_height(hgt):
    if "cm" in hgt:
        hgt = hgt.split('cm')
        hgtInt = int(hgt[0])
        if 150 <= hgtInt <= 193:
            return True
    elif "in" in hgt:
        hgt = hgt.split('in')
        hgtInt = int(hgt[0])
        if 59 <= hgtInt <= 76:
            return True
    return False

def check_hcl(hcl):
    if "#" in hcl:
        if len(hcl) == 7:
            hcl = hcl.split("#")[1]
            try:
                int(hcl, 16)
                return True
            except ValueError:
                return False
    return False

def check_ecl(ecl):
    if ecl == 'amb' or ecl == 'blu' or ecl == 'brn' or ecl == 'gry' or ecl == 'grn' or ecl == 'hzl' or ecl == 'oth':
        return True
    return False

def check_pid(pid):
    if len(pid) == 9:
        print("true")
        try:
            int(pid)
            return True
        except ValueError:
            return False
    return False

numberOfValidCards = 0
cardData = []
checking = True
for card in file:
    if "byr" in card and "iyr" in card and "eyr" in card and "hgt" in card and "hcl" in card and "ecl" in card and "pid" in card:
        checking = True
        cardData = re.split(' |\n', card)
        for i, elem in enumerate(cardData):
            if 'byr' in elem:
                byrI = i
            if 'iyr' in elem:
                iyrI = i
            if 'eyr' in elem:
                eyrI = i
            if 'hgt' in elem:
                hgtI = i
            if 'hcl' in elem:
                hclI = i
            if 'ecl' in elem:
                eclI = i
            if 'pid' in elem:
                pidI = i
        byr = int(cardData[byrI].split(":")[1])
        iyr = int(cardData[iyrI].split(":")[1])
        eyr = int(cardData[eyrI].split(":")[1])
        hgt = cardData[hgtI].split(":")[1]
        hcl = cardData[hclI].split(':')[1]
        ecl = cardData[eclI].split(':')[1]
        pid = cardData[pidI].split(':')[1]
        if 1920 <= byr <= 2002 and 2010 <= iyr <= 2020 and 2020 <= eyr <= 2030 and check_height(hgt) and check_hcl(hcl) and check_ecl(ecl) and check_pid(pid):
            numberOfValidCards += 1

print(numberOfValidCards)
